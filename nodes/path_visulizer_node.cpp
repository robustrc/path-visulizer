#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <opencv2/core/core.hpp>
#include "/usr/include/eigen3/Eigen/Dense"
#include <math.h>
#include <fenv.h>
#include <sstream>
#include <fstream>
#include <string>

using namespace cv;
//...ROS includes
#include "/opt/ros/lunar/include/ros/ros.h"
#include "/opt/ros/lunar/include/rospack/rospack.h"
#include "/opt/ros/lunar/include/tf/tf.h"
#include "/opt/ros/lunar/include/geometry_msgs/Vector3Stamped.h"
#include "/opt/ros/lunar/include/geometry_msgs/AccelStamped.h"
#include "/opt/ros/lunar/include/geometry_msgs/Point.h"

#include <rc_msgs/StateStamped.h>

#define DEG2RAD (M_PI/180)
#define RAD2DEG (180/M_PI)

void position0(const rc_msgs::StateStamped estState0);
void position1(const rc_msgs::StateStamped estState1);
void cam0(const geometry_msgs::Vector3Stamped camData0);
void cam1(const geometry_msgs::Vector3Stamped camData1);
void ref0(const geometry_msgs::Point refPoint0);
void ref1(const geometry_msgs::Point refPoint1);

Eigen::MatrixXf track;

rc_msgs::StateStamped pos[2];
geometry_msgs::Vector3Stamped cam[2];
geometry_msgs::Point refPoint[2];

float camPixelsWidth = 1200;
float camPixelsHeight = 800;

float camViewWidth = 4.29; // Width of the camera view, in meters.
float camViewHeight = 3.43; // Height of the camera view, in meters.

float px2mWidth = camViewWidth/camPixelsWidth; // Used to convert from pixel position to real world position
float px2mHeight = camViewHeight/camPixelsHeight; //


Eigen::MatrixXf loadCSV(const std::string & path);

int main( int argc, char** argv )
{
  // Init ROS
  ros::init(argc, argv, "path_visulizer");
  ros::NodeHandle n;
  ros::Rate loop_rate(100); // rate in Hz

  ros::Subscriber subPos0 = n.subscribe("estState0", 10, position0);
  ros::Subscriber subPos1 = n.subscribe("estState1", 10, position1);

  ros::Subscriber subRef0 = n.subscribe("refPoint0", 10, ref0);
  ros::Subscriber subRef1 = n.subscribe("refPoint1", 10, ref1);

  ros::Subscriber subCam0 = n.subscribe("car0PoseFromCam", 10, cam0);
  ros::Subscriber subCam1 = n.subscribe("car1PoseFromCam", 10, cam1);

  track  = loadCSV("/home/jlund/CONTROLLER/track.csv");

  //Create a black image
  Mat imgLines = Mat::zeros( camPixelsHeight, camPixelsWidth, CV_8UC3 );
  Mat imgOriginal = Mat::zeros( camPixelsHeight, camPixelsWidth, CV_8UC3 );
  Mat imgTrail = Mat::zeros( camPixelsHeight, camPixelsWidth, CV_8UC3 );

  for (size_t k = 0; k < track.rows(); k++) {
     circle(imgLines, Point(track(k,0)/px2mWidth,camPixelsHeight -track(k,1)/px2mHeight), 5, Scalar(200,200,200), -1, 8, 0);

if (false) {
  Point border[4];
  border[0] =  Point(217,132);
  border[1] =  Point(941,132);
  border[2] =  Point(941,668);
  border[3] =  Point(217,668);
  line(imgLines, border[0],
                 border[1],
                 Scalar(200,200,200), 2 ,0);
  line(imgLines, border[1],
                 border[2],
                 Scalar(200,200,200), 2 ,0);
  line(imgLines, border[2],
                 border[3],
                 Scalar(200,200,200), 2 ,0);
  line(imgLines, border[3],
                 border[0],
                 Scalar(200,200,200), 2 ,0);
}

  }
  imgOriginal = imgOriginal+ imgLines;

  int iX[2] = {-1,-1};
  int iY[2] = {-1,-1};

  int iLastX[2] = {-1,-1};
  int iLastY[2] = {-1,-1};

  int icamX[2] = {-1,-1};
  int icamY[2] = {-1,-1};

  int icamLastX[2] = {-1,-1};
  int icamLastY[2] = {-1,-1};

  while (ros::ok()) {
    for (size_t i = 0; i < 1; i++) {

      iX[i] = pos[i].px/px2mWidth;
      iY[i] = (camPixelsHeight - pos[i].py/px2mHeight);

      icamX[i] = cam[i].vector.x/px2mWidth;
      icamY[i] = (camPixelsHeight - cam[i].vector.y/px2mHeight);

      //Draw a red line from the previous point to the current point
      if (iX[i] > 0 && iY[i] > 0 && iLastX[i] > 0 && iLastY[i] > 0) {
        if (i == 0) {
          arrowedLine(imgLines, Point(iX[i], iY[i]), Point(iX[i] + 60*cos(pos[0].theta), iY[i] - 50*sin(pos[0].theta)),Scalar(0,0,200), 2 ,0.3);
          line(imgLines, Point(iX[i], iY[i]),
                         Point(iX[i] + 40*cos(pos[0].theta - refPoint[0].z), iY[i] - 40*sin(pos[0].theta - refPoint[0].z)),
                         Scalar(191,77,226), 1 ,0);
          line(imgLines, Point(iX[i], iY[i]),
                         Point(iX[i] + 40*cos(pos[0].theta), iY[i] - 40*sin(pos[0].theta)),
                         Scalar(191,77,226), 1 ,0);
          ellipse(imgLines, Point(iX[i], iY[i]), Size(40.0,40.0), -pos[0].theta*RAD2DEG, 0.0, refPoint[0].z*RAD2DEG, Scalar(191,77,226), 2, 8, 0);
          line(imgTrail, Point(iX[i], iY[i]), Point(iLastX[i], iLastY[i]), Scalar(0,0,255), 1);
          /*
          arrowedLine(imgLines, Point(iX[i] - 20*cos(pos[0].theta), iY[i] + 20*sin(pos[0].theta)),
                         Point(iX[i] + 20*cos(pos[0].theta), iY[i] - 20*sin(pos[0].theta)),
                         Scalar(0,0,200), 10 ,1);
          */
          Point carRect[4];
          carRect[0] =  Point(iX[i] + 20*cos(-pos[0].theta + M_PI/8), iY[i] + 20*sin(-pos[0].theta + M_PI/8));
          carRect[1] =  Point(iX[i] + 20*cos(-pos[0].theta + 2*M_PI - M_PI/8), iY[i] + 20*sin(-pos[0].theta + 2*M_PI - M_PI/8));
          carRect[2] =  Point(iX[i] + 20*cos(-pos[0].theta + M_PI +  M_PI/8), iY[i] + 20*sin(-pos[0].theta + M_PI + M_PI/8));
          carRect[3] =  Point(iX[i] + 20*cos(-pos[0].theta + M_PI -  M_PI/8), iY[i] + 20*sin(-pos[0].theta + M_PI -  M_PI/8));
          line(imgLines, carRect[0],
                         carRect[1],
                         Scalar(0,0,200), 2 ,0);
          line(imgLines, carRect[1],
                         carRect[2],
                         Scalar(0,0,200), 2 ,0);
          line(imgLines, carRect[2],
                         carRect[3],
                         Scalar(0,0,200), 2 ,0);
          line(imgLines, carRect[3],
                         carRect[0],
                         Scalar(0,0,200), 2 ,0);


        }
        if (i == 1) {
          line(imgLines, Point(iX[i], iY[i]), Point(iLastX[i], iLastY[i]), Scalar(255,100,0), 1);
        }
      }
      //Draw a red line from the previous point to the current point
      if (icamX[i] > 0 && icamY[i] > 0 && icamLastX[i] > 0 && icamLastY[i] > 0) {
        if (i == 0) {
          //line(imgLines, Point(icamX[i], icamY[i]), Point(icamX[i], icamY[i]), Scalar(0,0,155), 2);
          Point carRect[4];
          carRect[0] =  Point(icamX[i] + 20*cos(-cam[i].vector.z + M_PI/8), icamY[i] + 20*sin(-cam[i].vector.z + M_PI/8));
          carRect[1] =  Point(icamX[i] + 20*cos(-cam[i].vector.z + 2*M_PI - M_PI/8), icamY[i] + 20*sin(-cam[i].vector.z + 2*M_PI - M_PI/8));
          carRect[2] =  Point(icamX[i] + 20*cos(-cam[i].vector.z + M_PI +  M_PI/8), icamY[i] + 20*sin(-cam[i].vector.z + M_PI + M_PI/8));
          carRect[3] =  Point(icamX[i] + 20*cos(-cam[i].vector.z + M_PI -  M_PI/8), icamY[i] + 20*sin(-cam[i].vector.z + M_PI -  M_PI/8));
          line(imgLines, carRect[0],
                         carRect[1],
                         Scalar(0,200,0), 2 ,0);
         line(imgLines, carRect[1],
                         carRect[2],
                         Scalar(0,200,0), 2 ,0);
         line(imgLines, carRect[2],
                         carRect[3],
                         Scalar(0,200,0), 2 ,0);
         line(imgLines, carRect[3],
                        carRect[0],
                        Scalar(0,200,0), 2 ,0);
        }
        if (i == 1) {
          line(imgLines, Point(icamX[i], icamY[i]), Point(icamLastX[i], icamLastY[i]), Scalar(155,0,0), 2);
        }
      }
      if (refPoint[0].x != 0.0 && refPoint[0].y != 0.0) {
        circle(imgLines, Point(refPoint[0].x/px2mWidth,camPixelsHeight - refPoint[0].y/px2mHeight), 5, Scalar(0,255,0), 3, 8, 0);
      }

      imshow("Original", imgOriginal + imgLines + imgTrail); //show the original image
      imgTrail = imgTrail - Scalar(0,0,1);
      waitKey( 1 );
      imgLines = Mat::zeros( camPixelsHeight, camPixelsWidth, CV_8UC3 );
      iLastX[i] = iX[i];
      iLastY[i] = iY[i];
      icamLastX[i] = icamX[i];
      icamLastY[i] = icamY[i];
    }


    ros::spinOnce();
    //loop_rate.sleep();

  }
  return(0);
}

void position0(const rc_msgs::StateStamped estState0)
{
  pos[0] = estState0;
}

void position1(const rc_msgs::StateStamped estState1)
{
  pos[1] = estState1;
}

void cam0(const geometry_msgs::Vector3Stamped camData0)
{
  cam[0] = camData0;
}

void cam1(const geometry_msgs::Vector3Stamped camData1)
{
  cam[1] = camData1;
}
void ref0(const geometry_msgs::Point refPoint0)
{
  refPoint[0] = refPoint0;
}

void ref1(const geometry_msgs::Point refPoint1)
{
  refPoint[1] = refPoint1;
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
Eigen::MatrixXf loadCSV(const std::string & path)
{
  std::ifstream indata(path);
  std::vector <std::vector<float> > values;
  uint32_t rows = 0;
  uint cols = 0;
  bool runOnce = 1;
  while (indata) {
    std::string line;
    if (!getline( indata, line)) break;
    std::istringstream lineStream(line);
    std::vector <float> record;
    while (lineStream)
    {
      std::string s;
      if (!getline( lineStream, s, ',')) break;
      record.push_back( std::stof(s) );
      if (runOnce) cols = record.size();
    }
    values.push_back(record);
    runOnce = 0;
    ++rows;
  }
  Eigen::MatrixXf parsedCSV(rows,cols);
  for (size_t i = 0; i < cols; i++) {
    for (size_t j = 0; j < rows; j++) {
      parsedCSV(j,i) = values.at(j).at(i);
    }
  }
  return parsedCSV;
}
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
